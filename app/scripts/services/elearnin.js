/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amdTeacherApp')
/**
 * @ngdoc service
 * @name $seo
 * @memberof ngMaterialDashboardSeo
 * @description 
 * 
 * # Seo manager
 * 
 * Manages basic elements of SEo Base on Seen API.
 */
.service('$elearning', function($http, $pluf, PObjectFactory, ElDomain, ElTopic, ElCourse, ElLesson, ElPart) {

	var _domainCache = new PObjectFactory(function(data) {
		return new ElDomain(data);
	});
	var _topicCache = new PObjectFactory(function(data) {
		return new ElTopic(data);
	});
	var _courseCache = new PObjectFactory(function(data) {
		return new ElCourse(data);
	});
	var _lessonCache = new PObjectFactory(function(data) {
		return new ElLesson(data);
	});
	var _partCache = new PObjectFactory(function(data) {
		return new ElPart(data);
	});

	/**
	 * List domains
	 * 
	 * @memberof $eee
	 * @return {permision(PaginatedPage<ElDomain>)} list of domains
	 */
	this.domains = $pluf.createFind({
		method : 'GET',
		url : '/api/elearn/domain/find',
	}, _domainCache);

	/**
	 * Get an domain
	 * 
	 * @memberof $eee
	 * @param {object} id of the domain
	 * @return {promise<ElDomain>} the domain
	 */
	this.domain = $pluf.createGet({
		url : '/api/elearn/domain/{id}',
		method : 'GET'
	}, _domainCache);
	
	/**
	 * Creates new domain
	 * 
	 * @memberof $eee
	 * @param {Struct} data of an domain
	 * @return {promise<ElDomain>} created one
	 */
	this.newDomain = $pluf.createNew({
		method : 'POST',
		url : '/api/elearn/domain/new',
	}, _domainCache);
	
	
	
	

	/**
	 * List topics
	 * 
	 * @memberof $elearnin
	 * @return {permision(PaginatedPage<ElTopic>)} list of topics
	 */
	this.topics = $pluf.createFind({
		method : 'GET',
		url : '/api/elearn/topic/find',
	}, _topicCache);

	/**
	 * Get an topic
	 * 
	 * @memberof $elearnin
	 * @param {object} id of the topic
	 * @return {promise<ElTopic>} the topic
	 */
	this.topic = $pluf.createGet({
		url : '/api/elearn/topic/{id}',
		method : 'GET'
	}, _topicCache);
	
	/**
	 * Creates new topic
	 * 
	 * @memberof $elearnin
	 * @param {Struct} data of an topic
	 * @return {promise<ElTopic>} created one
	 */
	this.newTopic = $pluf.createNew({
		method : 'POST',
		url : '/api/elearn/topic/new',
	}, _topicCache);

	
	


	/**
	 * List courses
	 * 
	 * @memberof $elearnin
	 * @return {permision(PaginatedPage<ElCourse>)} list of courses
	 */
	this.courses = $pluf.createFind({
		method : 'GET',
		url : '/api/elearn/course/find',
	}, _courseCache);

	/**
	 * Get an course
	 * 
	 * @memberof $elearnin
	 * @param {object} id of the course
	 * @return {promise<ElCourse>} the course
	 */
	this.course = $pluf.createGet({
		url : '/api/elearn/course/{id}',
		method : 'GET'
	}, _courseCache);
	
	/**
	 * Creates new course
	 * 
	 * @memberof $elearnin
	 * @param {Struct} data of an course
	 * @return {promise<ElCourse>} created one
	 */
	this.newCourse = $pluf.createNew({
		method : 'POST',
		url : '/api/elearn/course/new',
	}, _courseCache);

	
	


	/**
	 * List lessons
	 * 
	 * @memberof $elearnin
	 * @return {permision(PaginatedPage<ElLesson>)} list of lesson
	 */
	this.lessons = $pluf.createFind({
		method : 'GET',
		url : '/api/elearn/lesson/find',
	}, _lessonCache);

	/**
	 * Get an lesson
	 * 
	 * @memberof $elearnin
	 * @param {object} id of the lesson
	 * @return {promise<ElLesson>} the lesson
	 */
	this.lesson = $pluf.createGet({
		url : '/api/elearn/lesson/{id}',
		method : 'GET'
	}, _lessonCache);
	
	/**
	 * Creates new lesson
	 * 
	 * @memberof $elearnin
	 * @param {Struct} data of an lesson
	 * @return {promise<ElLesson>} created one
	 */
	this.newLesson = $pluf.createNew({
		method : 'POST',
		url : '/api/elearn/lesson/new',
	}, _lessonCache);

	
	


	/**
	 * List parts
	 * 
	 * @memberof $elearnin
	 * @return {permision(PaginatedPage<ElPart>)} list of part
	 */
	this.parts = $pluf.createFind({
		method : 'GET',
		url : '/api/elearn/part/find',
	}, _partCache);

	/**
	 * Get an part
	 * 
	 * @memberof $elearnin
	 * @param {object} id of the part
	 * @return {promise<ElPart>} the part
	 */
	this.part = $pluf.createGet({
		url : '/api/elearn/part/{id}',
		method : 'GET'
	}, _partCache);
	
	/**
	 * Creates new part
	 * 
	 * @memberof $elearnin
	 * @param {Struct} data of an part
	 * @return {promise<ElPart>} created one
	 */
	this.newPart = $pluf.createNew({
		method : 'POST',
		url : '/api/elearn/part/new',
	}, _partCache);
	
	

});
