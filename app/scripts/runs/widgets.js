/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amdTeacherApp')
/**
 * load widgets
 */
.run(function($widget) {
	$widget.newWidget({
	    type: 'ElMultipleChoiceWidget',
	    templateUrl : 'views/widgets/choice-list.html',
	    label : 'Multiple Choice',
	    description : 'An widget to show multiple choice.',
	    image : 'images/wb/multiple-choice.svg',
	    help : 'https://gitlab.com/english-elearning-entertainment/eee-dashboard/wikis/home',
	    controller: 'ElMultipleChoiceCtrl',
	    setting:['multiple-choice-setting'],
	    data:{
	    	name: 'Multiple Choice',
	    	correct: 0,
	    	choices:[{
	            title: 'Choice 1',
	            value : 0,
	    		content : '<p>Text for choice 01</p>',
	            style : {}
	        }, {
	        	title: 'Choice 2',
	        	value : 1,
	            content : '<p>Text for choice 02</p>',
	            style : {}
	        }]
	    }
	});
});
