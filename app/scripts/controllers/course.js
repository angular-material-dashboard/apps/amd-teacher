/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amdTeacherApp')
/**
 * 
 */
.controller('ElCourseCtrl', function($scope, $navigator, $elearning, $routeParams, QueryParameter) {

	var paginatorParameter = new QueryParameter();
	paginatorParameter.setOrder('id', 'd');
	var requests = null;
	var ctrl = {
			state: 'ok',
			items: []
	};

	/**
	 * جستجوی درخواست‌ها
	 * 
	 * @param paginatorParameter
	 * @returns
	 */
	function find(query) {
		paginatorParameter.setQuery(query);
		paginatorParameter.setPage(0);
		reload();
	}

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.status === 'working') {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.status = 'working';
		$scope.course.lessons(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
			ctrl.status = 'ok';
		}, function(error) {
			ctrl.error = error;
			ctrl.status = 'fail';
		});
	}

	/**
	 * Adds new chiled
	 * @returns
	 */
	function addLesson(){
		$navigator.openPage('elearning/lessons/new');
	}


	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload(){
		$elearning.course($routeParams.id)//
		.then(function(course){
			$scope.course = course;
			requests = null;
			ctrl.items = [];
			nextPage();
		}, function(error){
			$scope.course = null;
			ctrl.status = 'notFound';
			ctrl.error = error;
		});
	}

	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove(object) {
		return object.delete()//
		.then(function(){
			var index = ctrl.items.indexOf(object);
			if (index > -1) {
				ctrl.items.splice(index, 1);
			}
		});
	}

	function removeCourse(){
		return $scope.course.delete()//
		.then(function(){
			$navigator.openPage('elearning/topics/'+$scope.course.topic);
		}, function(error){
			alert('Fail to remove course:' + error.data.emessage);
		});
	}

	$scope.items = [];
	$scope.reload = reload;
	$scope.search = find;
	$scope.nextPage = nextPage;

	$scope.remove = remove;
	$scope.removeCourse = removeCourse;
	$scope.add = addLesson;

	$scope.ctrl = ctrl;


	// Pagination toolbar
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	$scope.sortKeys= [
		'id', 
		'name',
		'description'
		];
	$scope.moreActions=[{
		title: 'New lesson',
		icon: 'add',
		action: addLesson
	}];
});
