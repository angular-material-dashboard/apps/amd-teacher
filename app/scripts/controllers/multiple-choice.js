/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amdTeacherApp')//
.controller('ElMultipleChoiceCtrl', function ($scope) {
	var wbModel = $scope.wbModel;

	function addChoice(){
		var count = wbModel.choices.length + 1;
		wbModel.choices.push({
			title: 'Choice ' + count,
            value : wbModel.choices.length,
    		content : '<p>Text for choice 0'+count+'</p>',
            style : {}
		});
	}
	
	function deleteChoice(index){
		wbModel.choices.splice(index, 1);
		for(var i = index ; i<wbModel.choices.length ; i++){
			wbModel.choices[i].value = i+1;
			wbModel.choices[i].title = 'Choice ' + (i+1);
		}
	}
	
	$scope.$watch('wbModel.choices', function () {
		wbModel.correct = -1;
	});
	
//	$scope.$watch('wbModel.autoplayTime', function () {
//		$interval.cancel(autoplayPromise);
//		autoplayPromise = $interval(function () {
//			if (wbModel.autoplay){
//				showNext();
//			}
//		}, wbModel.autoplayTime);
//	});
	
	$scope.addChoice = addChoice;
	$scope.deleteChoice = deleteChoice;
	
});