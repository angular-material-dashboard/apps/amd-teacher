/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amdTeacherApp')
/**
 * 
 */
.controller('ElPartCtrl', function($scope, $navigator, $elearning, $routeParams, QueryParameter) {

	var paginatorParameter = new QueryParameter();
	paginatorParameter.setOrder('id', 'd');
	var ctrl = {
			state: 'ok',
			items: []
	};

	function loadContent(){
		return $scope.part.value()//
		.then(function(value){
			$scope.model = value;
			if(!angular.isObject(value)){
				$scope.model = {};
			}
		}, function(error){
			alert('Fail to get content:' + error.data.message);
			$scope.model = {};
		});
	}
	
	function saveContent(){
		return $scope.part.setValue($scope.model)//
		.then(function(){
			alert('Part is saved.');
		}, function(error){
			alert('Fail to save the content:' + error.data.message);
		});
	}

	/**
	 * Load part
	 */
	function loadPart(){
		return $elearning.part($routeParams.id)//
		.then(function(part){
			$scope.part = part;
		}, function(error){
			$scope.part = null;
			ctrl.status = 'error';
			ctrl.error = error;
		});
	}
	
	function removePart(){
		return $scope.part.delete()//
		.then(function(){
			$navigator.openPage('elearning/lesson/'+$scope.part.lesson);
		}, function(error){
			alert('Fail to remove part:' + error.data.emessage);
		});
	}
	
	function updatePart(){
		return $scope.part.update();
	}
	
	

	/**
	 * 
	 * @returns
	 */
	function reload(){
		return loadPart().then(function(){
			return loadContent();
		});
	}

	$scope.loadContent = loadContent;
	$scope.saveContent = saveContent;
	$scope.loadContent = loadContent;
	
	$scope.reload = loadPart;
	$scope.remove = removePart;
	$scope.update = updatePart;

	$scope.ctrl = ctrl;

	reload();
});

