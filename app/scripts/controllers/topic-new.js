/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amdTeacherApp')
/**
 * 
 */
.controller('ElTopicNewCtrl', function($scope, $navigator, $elearning, QueryParameter) {


	var paginatorParameter = new QueryParameter();
	var ctrl = {
			state: 'ok',
	};

	/**
	 * Search for domain.
	 */
	function querySearch (query) {
		paginatorParameter.setQuery(query);
		return $elearning.domains(paginatorParameter)//
		.then(function(page){
			return page.items;
		});
	}
	
	/**
	 * Adds new topic
	 * @param model
	 * @returns
	 */
	function addTopic(model){
		model.domain = $scope.selectedDomian.id;
		return $elearning.newTopic(model)//
		.then(function(topic){
			$navigator.openPage('elearning/topics/'+topic.id);
		});
	}
	
	/**
	 * Go to topics list.
	 * 
	 * @returns
	 */
	function cancel(){
		$navigator.openPage('elearning/topics');
	}

	$scope.cancel = cancel;
	$scope.addTopic = addTopic;
	$scope.querySearch = querySearch;
	$scope.ctrl = ctrl;
});
