'use strict';

/*
 * Main module of the application.
 */
angular.module('amdTeacherApp', [ 
	'ngMaterialDashboardAccount',
	'ngMaterialDashboardUser',
	'ngMaterialDashboardCms',
	'FBAngular', // https://github.com/fabiobiondi/angular-fullscreen
])
//Load application
.run(function($app) {
	$app
	    .setDefaultToolbars(['dashboard'])
	    .setDefaultSidenavs(['navigator'])
	    .start('amd-teacher');
});
