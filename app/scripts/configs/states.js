/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amdTeacherApp')
/**
 * 
 */
.config(function($routeProvider) {
	$routeProvider//
	/*
	 * Dashboard
	 */
	.when('/elearning/domains', {
		templateUrl : 'views/elearning-domains.html',
		controller : 'ElDomainsCtrl',
		config : {
			groups : [ 'elearn' ],
			name : 'Domains',
			icon : 'elearning-domain',
			navigate : true,
			loginRequired : true
		}
	}).when('/elearning/domains/new', {
		templateUrl : 'views/elearning-domain-new.html',
		controller : 'ElDomainNewCtrl',
		config : {
			groups : [ 'elearn' ],
			name : 'Domains',
			icon : 'elearning-domain',
			navigate : false,
			loginRequired : true
		}
	}).when('/elearning/domains/:id', {
		templateUrl : 'views/elearning-domain.html',
		controller : 'ElDomainCtrl',
		config : {
			groups : [ 'elearn' ],
			name : 'Domains',
			icon : 'elearning-domain',
			navigate : false,
			loginRequired : true
		}
	})

	.when('/elearning/topics', {
		templateUrl : 'views/elearning-topics.html',
		controller : 'ElTopicsCtrl',
		config : {
			groups : [ 'elearn' ],
			name : 'Topics',
			icon : 'elearning-topic',
			navigate : true,
			loginRequired : true
		}
	}).when('/elearning/topics/new', {
		templateUrl : 'views/elearning-topic-new.html',
		controller : 'ElTopicNewCtrl',
		config : {
			navigate : false,
			loginRequired : true
		}
	}).when('/elearning/topics/:id', {
		templateUrl : 'views/elearning-topic.html',
		controller : 'ElTopicCtrl',
		config : {
			navigate : false,
			loginRequired : true
		}
	})

	.when('/elearning/courses', {
		templateUrl : 'views/elearning-courses.html',
		controller : 'ElCoursesCtrl',
		config : {
			groups : [ 'elearn' ],
			name : 'Courses',
			icon : 'elearning-course',
			navigate : true,
			loginRequired : true
		}
	}).when('/elearning/courses/new', {
		templateUrl : 'views/elearning-course-new.html',
		controller : 'ElCourseNewCtrl',
		config : {
			navigate : false,
			loginRequired : true
		}
	}).when('/elearning/courses/:id', {
		templateUrl : 'views/elearning-course.html',
		controller : 'ElCourseCtrl',
		config : {
			navigate : false,
			loginRequired : true
		}
	})

	.when('/elearning/lessons', {
		templateUrl : 'views/elearning-lessons.html',
		controller : 'ElLessonsCtrl',
		config : {
			groups : [ 'elearn' ],
			name : 'Lessons',
			icon : 'elearning-lessons',
			navigate : true,
			loginRequired : true
		}
	}).when('/elearning/lessons/new', {
		templateUrl : 'views/elearning-lesson-new.html',
		controller : 'ElLessonNewCtrl',
		config : {
			navigate : false,
			loginRequired : true
		}
	}).when('/elearning/lessons/:id', {
		templateUrl : 'views/elearning-lesson.html',
		controller : 'ElLessonCtrl',
		config : {
			navigate : false,
			loginRequired : true
		}
	})

	.when('/elearning/parts', {
		templateUrl : 'views/elearning-parts.html',
		controller : 'ElPartsCtrl',
		config : {
			groups : [ 'elearn' ],
			name : 'Parts',
			icon : 'elearning-part',
			navigate : true,
			loginRequired : true
		}
	}).when('/elearning/parts/new', {
		templateUrl : 'views/elearning-part-new.html',
		controller : 'ElPartNewCtrl',
		config : {
			navigate : false,
			loginRequired : true
		}
	}).when('/elearning/parts/:id', {
		templateUrl : 'views/elearning-part.html',
		controller : 'ElPartCtrl',
		config : {
			navigate : false,
			loginRequired : true
		}
	}).when('/elearning/parts/:id/edit', {
		templateUrl : 'views/elearning-part-edit.html',
		controller : 'ElPartCtrl',
		config : {
			navigate : false,
			loginRequired : true
		}
	});
});
