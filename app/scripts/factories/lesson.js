/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amdTeacherApp')
/**
 * @ngdoc factory
 * @memberof eeeDashboardApp
 * @name ElDomain
 * @description 
 * 
 * # ElDomain data model
 * 
 */
.factory('ElLesson', function(PObject, $pluf, PObjectFactory, ElPart) {
	var _partCache = new PObjectFactory(function(data) {
		return new ElPart(data);
	});
	var lesson = function() {
		PObject.apply(this, arguments);
	};
	lesson.prototype = new PObject();

	/**
	 * Delete the model
	 * 
	 * @memberof ElLesson
	 * @return {promise<ElLesson>} 
	 */
	lesson.prototype.delete = $pluf.createDelete({
		method : 'DELETE',
		url : '/api/elearn/lesson/:id'
	});

	/**
	 * Update the model
	 * 
	 * @memberof ElLesson
	 * @return {promise<ElLesson>} 
	 */
	lesson.prototype.update =  $pluf.createUpdate({
		method : 'POST',
		url : '/api/elearn/lesson/:id',
	});
	
	lesson.prototype.parts = $pluf.createFind({
		method : 'GET',
		url : '/api/elearn/lesson/:id/part/find',
	}, _partCache);
	
	return lesson;
});
