/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amdTeacherApp')
/**
 * @ngdoc factory
 * @memberof eeeDashboardApp
 * @name ElDomain
 * @description 
 * 
 * # ElDomain data model
 * 
 */
.factory('ElPart', function(PObject, $pluf, $http) {
	var part = function() {
		PObject.apply(this, arguments);
	};
	part.prototype = new PObject();

	/**
	 * Delete the model
	 * 
	 * @memberof ElPart
	 * @return {promise<ElPart>} 
	 */
	part.prototype.delete = $pluf.createDelete({
		method : 'DELETE',
		url : '/api/elearn/part/:id'
	});

	/**
	 * Update the model
	 * 
	 * @memberof ElPart
	 * @return {promise<ElPart>} 
	 */
	part.prototype.update =  $pluf.createUpdate({
		method : 'POST',
		url : '/api/elearn/part/:id',
	});

	/**
	 * Gets content of the part
	 * 
	 * @memberof PContent
	 * @return {promise} مقدار محتوی
	 */
	part.prototype.value =function() {
		return $http({
			method : 'GET',
			url : '/api/elearn/part/'+this.id+'/content',
		})//
		.then(function(resp){
			return resp.data;
		});
	};

	/**
	 * Sets content of the parts
	 * 
	 * @memberof PContent
	 * @param {object}
	 *            data مقدار جدید برای محتوی
	 * @return {promise} محتوی به روز شده
	 */
	part.prototype.setValue = function(newValue) {
		var scope = this;
		return $http({
			method : 'POST',
			url : '/api/elearn/part/'+scope.id+'/content',
			data : newValue,
		}).then(function() {
			return scope;
		});
	};

	/**
	 * Uploads a file.
	 * 
	 * @param file
	 * @returns
	 */
	part.prototype.upload = function(file) {
		var fd = new FormData();
		fd.append('file', file);
		return $http.post(this.link, fd, {
			transformRequest : angular.identity,
			headers : {
				'Content-Type' : undefined
			}
		});
	};

	return part;
});
